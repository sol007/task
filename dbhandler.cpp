#include "dbhandler.h"
#include <QDebug>

DbHandler::DbHandler(QObject *parent) : QObject(parent)
{
    cellProviders=new QSqlDatabase(QSqlDatabase::addDatabase("QSQLITE"));
    cellProviders->setDatabaseName("./system.db");
    if(!cellProviders->open()){
        qDebug()<<"Error opening database "<<cellProviders->lastError();
    }
}

DbHandler::~DbHandler()
{
    cellProviders->close();
    delete cellProviders;

}

void DbHandler::fillCountriesList(TreeItem *rootItem)
{
    QSqlQuery query;
    if(!query.exec("SELECT * FROM countries")){
        qDebug()<<"Error execute statemant "<<query.executedQuery()<<query.lastError();
    }
    else while(query.next()){
         rootItem->appendChild(new TreeItem(query.value(2).toString(),query.value(0).toInt(),-1,query.value(1).toString(),rootItem));// mnc=-1 always for country group

    }
}

void DbHandler::fillOperatorsList(TreeItem *country)
{
    QSqlQuery query;
    if(!query.exec("SELECT * FROM operators WHERE mcc="+QString::number(country->getMcc()))){
        qDebug()<<"Error execute statemant "<<query.executedQuery()<<query.lastError();
    }
    else while(query.next()){
        country->appendChild(new TreeItem(query.value(2).toString(),query.value(0).toInt(),query.value(1).toInt(),QString(),country));
    }
}

void DbHandler::updateOperatorName(QString newName, QString mcc, QString mnc)
{
    QSqlQuery query;
    QString qrg="UPDATE operators SET name='"+newName+"' WHERE mcc="+mcc+" AND mnc="+mnc;
    //qDebug()<<qrg;
    if(!query.exec(qrg)){
        qDebug()<<"Error execute statemant "<<query.executedQuery()<<query.lastError();
    }
}

QString DbHandler::getCountryPrefixByMcc(QString mcc)
{
    QSqlQuery query;
    if(!query.exec("SELECT code FROM countries WHERE mcc="+mcc)){
        qDebug()<<"Error execute statemant "<<query.executedQuery()<<query.lastError();
    }
    else while(query.next()){
        return query.value(0).toString();
    }
    return QString();
}

void DbHandler::addNewOperator(QString newName, QString mcc, QString mnc)
{
    QSqlQuery query;
    QString qrg="INSERT INTO operators (mcc,mnc,name)"
                " VALUES('"+mcc+"','"+mnc+"','"+newName+"')";
    qDebug()<<qrg;
    if(!query.exec(qrg)){
        qDebug()<<"Error execute statemant "<<query.executedQuery()<<query.lastError();
    }

}

