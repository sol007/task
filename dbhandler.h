#ifndef DBHANDLER_H
#define DBHANDLER_H

#include <QObject>
#include <QtSql/QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QList>
#include <treeitem.h>

class DbHandler : public QObject
{
    Q_OBJECT
public:
    explicit DbHandler(QObject *parent = nullptr);
    ~DbHandler() override;
    void fillCountriesList(TreeItem *rootItem);
    void fillOperatorsList(TreeItem* country);
    void updateOperatorName(QString newName,QString mcc, QString mnc);
    QString getCountryPrefixByMcc(QString mcc);
    void addNewOperator(QString newName,QString mcc, QString mnc);
signals:

public slots:

private:
    QSqlDatabase *cellProviders;

};

#endif // DBHANDLER_H
