#include "dialog.h"
#include "ui_dialog.h"
#include <QFile>
#include <QDebug>

Dialog::Dialog(DbHandler *dbHandler,QWidget *parent) : QDialog(parent), dbHandler(dbHandler),ui(new Ui::Dialog)
{
    ui->setupUi(this);
    setWindowFlags(Qt::CustomizeWindowHint);
    connect(ui->MCCEdir,SIGNAL(textEdited(QString)),this,SLOT(changedMncMcc(QString)));
    connect(ui->MNCEdit,SIGNAL(textEdited(QString)),this,SLOT(changedMncMcc(QString)));
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::setMncFieldActive(bool state)
{
    ui->MNCEdit->setEnabled(state);
}

void Dialog::setMccFieldActive(bool state)
{
    ui->MCCEdir->setEnabled(state);
}

void Dialog::setNameFieldValue(QString value)
{
    ui->OperatorEdit->setText(value);
}

void Dialog::setMccFiedValue(QString value)
{
    ui->MCCEdir->setText(value);
}

void Dialog::setMncFieldValue(QString value)
{
    ui->MNCEdit->setText(value);
}

void Dialog::setCountryIcon(QString prefix)
{
    QString file(QString("./Icons/%1.png").arg(prefix));
    if(QFile::exists(file))ui->countryPixmap->setPixmap(file);
    else ui->countryPixmap->setPixmap(QString("./Icons/question.png"));
}

QString Dialog::getOpertor()
{
    return ui->OperatorEdit->text();
}

QString Dialog::getMCC()
{
    return  ui->MCCEdir->text();
}

QString Dialog::getMNC()
{
    return  ui->MNCEdit->text();
}

void Dialog::changedMncMcc(QString text)
{
   Q_UNUSED(text)
    setOperatorIcon(ui->MCCEdir->text(),ui->MNCEdit->text());
    if(!ui->MCCEdir->text().isEmpty())setCountryIcon(dbHandler->getCountryPrefixByMcc(ui->MCCEdir->text()));
}

void Dialog::setOperatorIcon(QString mcc,QString mnc)
{
    QString file(QString("./Icons/%1_%2.png").arg(mcc).arg(mnc));
    if(QFile::exists(file))ui->operatorPixmap->setPixmap(file);
    else ui->operatorPixmap->setPixmap(QString("./Icons/question.png"));
}
