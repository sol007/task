#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include "dbhandler.h"

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(DbHandler *dbHandler, QWidget *parent = nullptr);
    ~Dialog();
    void setMncFieldActive(bool state);
    void setMccFieldActive(bool state);
    void setNameFieldValue(QString value);
    void setMccFiedValue(QString value);
    void setMncFieldValue(QString value);
    void setOperatorIcon(QString mcc,QString mnc);
    void setCountryIcon(QString prefix);
    QString getOpertor();
    QString getMCC();
    QString getMNC();


private:
    Ui::Dialog *ui;
    DbHandler *dbHandler;
    private slots:
    void  changedMncMcc(QString text);
};

#endif // DIALOG_H
