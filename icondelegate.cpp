#include "icondelegate.h"
#include <QPainter>

#include "QDebug"
#include <QPointF>
#include <QPushButton>
#define SPACING 6

IconDelegate::IconDelegate(QPushButton *btn,QObject *parent):QStyledItemDelegate(parent),btn(btn)
{
}

IconDelegate::~IconDelegate()
{

}

void IconDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    TreeItem *item = static_cast<TreeItem*>(index.internalPointer());
     QImage image;
     QString data;
     if(item->getMnc()!=-1){image.load(QString("./Icons/%1_%2.png").arg(item->getMcc()).arg(item->getMnc()));
        data=QString("%1 (%2,%3)").arg(item->data()).arg(item->getMcc()).arg(item->getMnc());}
     else {image.load(QString("./Icons/%1.png").arg(item->getIconPrefix()));
        data=item->data();}
    painter->drawImage(QRect(option.rect.left(),option.rect.y()+option.rect.height()/2-image.size().height()/2,image.size().width(),image.size().height()),image);
    QRect boundingRect;
    painter->drawText(QRect(option.rect.x()+image.width()+SPACING,option.rect.y(),option.rect.width(),option.rect.height()),Qt::AlignLeft|Qt::AlignVCenter,data,&boundingRect);
    if (option.state & QStyle::State_MouseOver) {
       btn->move(boundingRect.right()+SPACING,boundingRect.bottom()+10);
       if(item->getMnc()!=-1){
           btn->show();}
       else btn->hide();
   }
    QStyledItemDelegate::paint(painter, option, index);
}

