#ifndef ICONDELEGATE_H
#define ICONDELEGATE_H

#include <QObject>
#include <QStyledItemDelegate>
#include <QPushButton>
#include "treeitem.h"

class IconDelegate : public QStyledItemDelegate
{
    Q_OBJECT

public:
    IconDelegate(QPushButton *btn,QObject *parent =nullptr);
    ~IconDelegate() override;
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
    //QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const override;
   signals:
    void dataSignal(int mcc, int mnc);
private :
    QPushButton *btn;
    TreeItem *currItem=nullptr;
  private  slots:

};

#endif // ICONDELEGATE_H
