#include "mywidget.h"
#include "QDebug"
#include "dialog.h"

MyWidget::MyWidget(QWidget *parent) : QWidget(parent)
{

    layout=new QHBoxLayout(this);
    dbHdl=new DbHandler(this);
    worldTree=new WorldModelTree(dbHdl,this);
    treeView=new QTreeView(this);
    btn=new QPushButton("+",treeView);
    btn->setFixedSize(QSize(15,15));
    btn->hide();
    newBtn=new QPushButton("+",treeView);
    newBtn->setFixedSize(QSize(15,15));
    delegate=new IconDelegate(btn,treeView);
    treeView->setItemDelegate(delegate);
    treeView->setModel(worldTree);
    treeView->viewport()->setAttribute(Qt::WA_Hover);
    treeView->setEditTriggers(QAbstractItemView::CurrentChanged);
    treeView->setMouseTracking(true);
    layout->addWidget(treeView);
    connect(treeView,SIGNAL(entered(QModelIndex)),this,SLOT(activateSlot(QModelIndex)));
    connect(treeView,SIGNAL(doubleClicked(QModelIndex)),this,SLOT(showDialogSlot(QModelIndex)));
    connect(btn,SIGNAL(clicked()),this,SLOT(btnclicked()));
    connect(this,SIGNAL(btnClicked(int,int)),this,SLOT(emptySlot(int,int)));
    connect(newBtn,SIGNAL(clicked()),this,SLOT(newBtnclicked()));
    connect(this,SIGNAL(newOperators(QString,int,int)),worldTree,SLOT(addNewOperator(QString,int,int)));

}

MyWidget::~MyWidget()
{
    delete treeView;
    delete worldTree;
    delete layout;
    delete delegate;
    delete btn;
    delete dbHdl;

}

void MyWidget::activateSlot(QModelIndex index)
{
    activeItem = static_cast<TreeItem*>(index.internalPointer());
}

void MyWidget::btnclicked()
{
    emit btnClicked(activeItem->getMcc(),activeItem->getMnc());
}

void MyWidget::newBtnclicked()
{
    Dialog dialog(dbHdl);

    dialog.setOperatorIcon(QString(),QString());
    dialog.setCountryIcon(QString());
    dialog.setMccFieldActive(true);
    dialog.setMncFieldActive(true);
    if(dialog.exec()==QDialog::Accepted){
      emit newOperators(dialog.getOpertor(),dialog.getMCC().toInt(),dialog.getMNC().toInt());
    }
    treeView->update();
}

void MyWidget::emptySlot(int mcc, int mnc)
{
    qDebug()<<"Empty slot called mcc="<<mcc<< ", mnc="<<mnc;
}

void MyWidget::showDialogSlot(QModelIndex index)
{
   TreeItem *item = static_cast<TreeItem*>(index.internalPointer());
   if(item->getMnc()!=-1)
   {Dialog dialog(dbHdl);
   dialog.setNameFieldValue(item->data());
   dialog.setMccFiedValue(QString::number(item->getMcc()));
   dialog.setMncFieldValue(QString::number(item->getMnc()));
   dialog.setOperatorIcon(QString::number(item->getMcc()),QString::number(item->getMnc()));
   dialog.setCountryIcon(item->parentItem()->getIconPrefix());
   if(dialog.exec()==QDialog::Accepted&&item->data()!=dialog.getOpertor()){
       dbHdl->updateOperatorName(dialog.getOpertor(),dialog.getMCC(),dialog.getMNC());
       item->setData(dialog.getOpertor());
       treeView->update();
   }
   }
}

void MyWidget::resizeEvent(QResizeEvent *event)
{
    newBtn->move(treeView->width()-35,treeView->height()-20);
    QWidget::resizeEvent(event);

}

