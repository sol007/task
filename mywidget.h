#ifndef MYWIDGET_H
#define MYWIDGET_H

#include <QObject>
#include <QWidget>
#include <QHBoxLayout>
#include "QTreeView"
#include "dbhandler.h"
#include "worldmodeltree.h"
#include "icondelegate.h"
#include "QPushButton"

class MyWidget : public QWidget
{
    Q_OBJECT
public:
    explicit MyWidget(QWidget *parent = nullptr);
    ~MyWidget() override;
    DbHandler *dbHdl;
signals:
    void btnClicked(int mcc,int mnc);
    void newOperators(QString name,int mcc,int mnc);
public slots:
private:
    QHBoxLayout *layout;
     WorldModelTree *worldTree;
     QTreeView *treeView;
     IconDelegate *delegate;
     QPushButton *btn;
     QPushButton *newBtn;
     TreeItem *activeItem;
private slots:
     void activateSlot(QModelIndex index);
     void btnclicked();
     void newBtnclicked();
     void emptySlot(int mcc,int mnc);
     void showDialogSlot(QModelIndex index);

protected:
     void resizeEvent(QResizeEvent *event) override;
};

#endif // MYWIDGET_H
