#include "treeitem.h"

TreeItem::TreeItem(QString itemName,int mcc,int mnc,QString iconPrefix,TreeItem* parent):itemName(itemName),parent(parent),mcc(mcc),mnc(mnc),iconPrefix(iconPrefix)
{

}

TreeItem::~TreeItem()
{
    qDeleteAll(childItems);
}

TreeItem *TreeItem::child(int row)
{
    return childItems.value(row);
}

int TreeItem::childCount() const
{
    return childItems.count();
}

QString TreeItem::data() const
{

    return itemName;
}

void TreeItem::setData(const QString data)
{
    itemName=data;
}

int TreeItem::row() const
{
    if (parent)
            return parent->childItems.indexOf(const_cast<TreeItem*>(this));
    return 0;
}

TreeItem *TreeItem::parentItem()
{
    return parent;
}
void TreeItem::appendChild(TreeItem *item)
{
    childItems.append(item);
}

int TreeItem::getMcc() const
{
    return mcc;
}

int TreeItem::getMnc() const
{
    return mnc;
}

QString TreeItem::getIconPrefix() const
{
    return iconPrefix;
}


