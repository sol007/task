#ifndef TREEITEM_H
#define TREEITEM_H
#include "QString"
#include <QList>

class TreeItem
{
public:
    TreeItem(QString itemName,int mcc=-1,int mnc=-1,QString iconPrefix="",TreeItem* parent =nullptr );
    ~TreeItem();

    TreeItem *child(int row);
    int childCount() const;
    QString data() const;
    void setData(QString itemName);
    int row() const;
    TreeItem *parentItem();
    void appendChild(TreeItem *item);


    int getMcc() const;

    int getMnc() const;

    QString getIconPrefix() const;

private:
    QList<TreeItem*> childItems;
    QString itemName;
    TreeItem *parent;
    int mcc=-1;
    int mnc=-1;
    QString iconPrefix;
};

#endif // TREEITEM_H
