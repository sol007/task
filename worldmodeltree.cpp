#include "worldmodeltree.h"
#include "QDebug"

WorldModelTree::WorldModelTree(DbHandler *dbHdl,QObject *parent):QAbstractItemModel(parent),dbHdl(dbHdl)
{

  rootItem=new TreeItem("Countries");
  dbHdl->fillCountriesList(rootItem);
  for(int i=0;i<rootItem->childCount();i++)
  {dbHdl->fillOperatorsList(rootItem->child(i)); }
}

WorldModelTree::~WorldModelTree()
{
    delete rootItem;

}
QVariant WorldModelTree::headerData(int section, Qt::Orientation orientation,
                               int role) const
{

    Q_UNUSED(section)
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
        return QVariant(rootItem->data());

   return QVariant();
}
QVariant WorldModelTree::data(const QModelIndex &index, int role) const
{

    /*if (!index.isValid())
            return QVariant();
    if (role != Qt::DisplayRole)
     return QVariant();
    TreeItem *item=static_cast<TreeItem*>(index.internalPointer());
    if(item->getMnc()!=-1)return QString("%1 (%2,%3)").arg(item->data()).arg(item->getMcc()).arg(item->getMnc());
        return item->data();*/
    return QVariant();
}
Qt::ItemFlags WorldModelTree::flags(const QModelIndex &index) const
{

    if (!index.isValid())
        return Qt::NoItemFlags;
    return Qt::ItemIsEnabled|Qt::ItemIsSelectable;
}

void WorldModelTree::addNewOperator(QString name, int mcc, int mnc)
{

    for(int i=0;i<rootItem->childCount();i++)
    {
        if(rootItem->child(i)->getMcc()==mcc){
            rootItem->child(i)->appendChild(new TreeItem(name,mcc,mnc,QString(),rootItem->child(i)));
             insertRows(rootItem->child(i)->childCount()-1,1,createIndex(rootItem->child(i)->row(),0,rootItem->child(i)));
        dbHdl->addNewOperator(name,QString::number(mcc),QString::number(mnc));
        }
    }
}
QModelIndex WorldModelTree::index(int row, int column, const QModelIndex &parent) const
{

    if (!hasIndex(row, column, parent))
           return QModelIndex();

       TreeItem *parentItem;

       if (!parent.isValid())
           parentItem = rootItem;
       else
           parentItem = static_cast<TreeItem*>(parent.internalPointer());

       TreeItem *childItem = parentItem->child(row);
       if (childItem)
           return createIndex(row, column, childItem);
       else
           return QModelIndex();

}

QModelIndex WorldModelTree::parent(const QModelIndex &index) const
{

    if (!index.isValid())
        return QModelIndex();

    TreeItem *childItem = static_cast<TreeItem*>(index.internalPointer());
    TreeItem *parentItem = childItem->parentItem();

    if (parentItem== rootItem)
        return QModelIndex();

    return createIndex(parentItem->row(), 0, parentItem);

}

int WorldModelTree::rowCount(const QModelIndex &parent) const
{

    if (!parent.isValid())
       {
        return rootItem->childCount();}
    else{

     return static_cast<TreeItem*>(parent.internalPointer())->childCount();}

}

int WorldModelTree::columnCount(const QModelIndex &parent) const
{

    Q_UNUSED(parent)
    return 1; //only 1 column avilable in our treeModel
}
bool WorldModelTree::insertRows(int position, int rows, const QModelIndex &parent)
{

    TreeItem *parentItem = static_cast<TreeItem*>(parent.internalPointer());
    if (!parentItem)
        return false;

    beginInsertRows(parent, position, position + rows - 1);
    //const bool success = parentItem->appendChild()
    endInsertRows();

    return true;
}
