#ifndef WORLDMODELTREE_H
#define WORLDMODELTREE_H

#include <QObject>
#include <QAbstractItemModel>
#include <QModelIndex>
#include <QVariant>
#include "dbhandler.h"
#include "treeitem.h"
class TreeItem;
class WorldModelTree: public QAbstractItemModel
{
     Q_OBJECT

public:
    explicit WorldModelTree(DbHandler *dbHdl,QObject *parent = nullptr);
    ~WorldModelTree() override;
    QVariant data(const QModelIndex &index, int role) const override;
   QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
   QModelIndex parent(const QModelIndex &index) const override;
   int rowCount(const QModelIndex &parent = QModelIndex()) const override;
   int columnCount(const QModelIndex &parent = QModelIndex()) const override;
   QVariant headerData(int section, Qt::Orientation orientation,
                           int role = Qt::DisplayRole) const override;
   Qt::ItemFlags flags(const QModelIndex &index) const override;
   bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

public slots :
   void addNewOperator(QString name,int mcc,int mnc);
private:
   DbHandler *dbHdl;
   TreeItem *rootItem;
};

#endif // WORLDMODELTREE_H
